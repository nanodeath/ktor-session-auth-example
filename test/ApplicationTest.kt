package name.maxaller

import io.ktor.http.*
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import name.maxaller.ApplicationResponseAssert.Companion.assertThat
import kotlin.test.Test

class ApplicationTest {
    @Test
    fun testRoot() {
        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Get, "/").apply {
                assertThat(response).redirectsTo("/login")
            }
        }
    }

    @Test
    fun testAuth() {
        withTestApplication({ module(testing = true) }) {
            cookiesSession {
                authenticate()
            }
        }
    }

    @Test
    fun testAuthFailure() {
        withTestApplication({ module(testing = true) }) {
            cookiesSession {
                handleRequest(HttpMethod.Post, "/login") {
                    addHeader(HttpHeaders.ContentType, ContentType.Application.FormUrlEncoded.toString())
                    setBody(listOf("username" to "foo", "password" to "iforgot").formUrlEncode())
                }.apply {
                    assertThat(response).redirectsTo("/login?invalid")
                }
            }
        }
    }

    @Test
    fun testNeedToAuthFirst() {
        withTestApplication({ module(testing = true) }) {
            cookiesSession {
                handleRequest(HttpMethod.Get, "/profile").apply {
                    assertThat(response).redirectsTo("/login?no")
                }
            }
        }
    }

    @Test
    fun testCanAccessProfileAfterAuth() {
        withTestApplication({ module(testing = true) }) {
            cookiesSession {
                authenticate()
                handleRequest(HttpMethod.Get, "/profile").apply {
                    assertThat(response).hasStatus(HttpStatusCode.OK)
                }
            }
        }
    }

    @Test
    fun testCanUnAuthenticate() {
        withTestApplication({ module(testing = true) }) {
            cookiesSession {
                authenticate()
                unauthenticate()
                handleRequest(HttpMethod.Get, "/profile").apply {
                    assertThat(response).redirectsTo("/login?no")
                }
            }
        }
    }

    @Test
    fun testRootAuth() {
        withTestApplication({ module(testing = true) }) {
            cookiesSession {
                authenticate()
                handleRequest(HttpMethod.Get, "/").apply {
                    assertThat(response).redirectsTo("/profile")
                }
            }
        }
    }

    private fun CookieTrackerTestApplicationEngine.authenticate() {
        handleRequest(HttpMethod.Get, "/login").apply {
            assertThat(response).hasStatus(HttpStatusCode.OK)
        }
        handleRequest(HttpMethod.Post, "/login") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.FormUrlEncoded.toString())
            setBody(listOf("username" to "foo", "password" to "bar").formUrlEncode())
        }.apply {
            assertThat(response).redirectsTo("/profile")
        }
    }

    private fun CookieTrackerTestApplicationEngine.unauthenticate() {
        handleRequest(HttpMethod.Get, "/logout").apply {
            assertThat(response).redirectsTo("/login")
        }
    }
}

