# Ktor Session Auth Example

The purpose of this application is to serve as a simple example of how to use [Ktor](https://ktor.io/)'s
[authentication](https://ktor.io/servers/features/authentication.html) and [sessions](https://ktor.io/servers/features/sessions.html)
modules to construct a "typical" webapp:
1. User goes to a login page.
2. User inputs their credentials.
3. User is redirected to a signed-in landing page, and the site remembers them from request to request.

Other common cases are also handled:
* User tries going directly to a signed-in page without signing in first.
  * This will redirect the user to the sign-in page.
* User tries to authenticate, but does so incorrectly. 
  * This also redirects the user to the sign-in page, but with an error query param.
* User can log out.
  * Destroys the auth cookie.

It's well-documented and contains unit tests.

## Running the Application

* Option 1: `./gradlew run`.
* Option 2: Run the `main` method in Application.kt from your IDE.

Navigate to the URL printed out by ktor.

To run tests, `./gradlew test` or use your IDE.

## Authentication in Ktor

*Read the [Authentication](https://ktor.io/servers/features/authentication.html) documentation first.*

Basically, you need to do two things:
* Install 1 or more authentication providers to guard your routes.
* Guard your routes using 1 or more of those authentication modules.

Installing providers is easy and is covered in the official documentation -- you want the `form` provider (for handling
the postback from your login page) and the `session` provider to follow the user, so they don't need to reauthenticate
on every request.

Usually your auth modules are named so you can say "this route requires authentication using X module", but you can omit
the name from a single authentication provider if you like.

In this example application, both the `form` provider and the `session` provider have names, which means when guarding
your routes (using `authenticate {}`) you must specify those names. In a real application, I'd leave the name off the
`session` provider because that's what I plan on using everywhere except the login page.

### `form` provider

The [`form` provider](https://ktor.io/servers/features/authentication/basic.html) is pretty well documented at ktor.io
and within this app.

The only place you need to use the `form` provider is on any page that your login page POSTs to -- generally just your
`POST /login` route. It `validate`s the user input, and if successful, lets them hit your actual route, and if not,
it `challenge`s them to try again.

### `session` provider

The [`session` provider](https://github.com/ktorio/ktor/blob/19054d036f6ee9dc89ae982c96efe900221ae39b/ktor-features/ktor-auth/jvm/src/io/ktor/auth/SessionAuth.kt)
is, surprisingly, not actually documented on ktor.io, but the source is fairly readable. In particular, the
[CheckAuthentication interceptor](https://github.com/ktorio/ktor/blob/19054d036f6ee9dc89ae982c96efe900221ae39b/ktor-features/ktor-auth/jvm/src/io/ktor/auth/SessionAuth.kt#L154-L172)
is key to understand and also relatively simple.

Whatever the type of the `session` provider, it's expected that exact type is also stored as a cookie.

The `session` provider doesn't do anything to initialize a session -- your `POST /login` route needs to save the cookie
itself.

## Sessions in Ktor

*Read the [Sessions](https://ktor.io/servers/features/sessions.html) documentation first.*

Sessions can be implemented in a variety of ways which are covered in the documentation above.

In this application, sessions are configured to use cookies (common for browser-based web applications) and server cookies
backed by the very-simple `SessionStorageMemory`.

`SessionStorageMemory` isn't sufficient for production use cases:
* It never expires sessions (no TTL)
* It forgets sessions when the server restarts
* If your fleet has multiple hosts, only the fleet the user signed into will actually recognize the user.

Thus if you're interested in using server cookies you'll almost certainly need to use a persistent shared storage
mechanism like a database.

## Where to go from here

If you've replicated the auth in this application, here are some other things you'll need to do eventually:
* Implement real authentication, using a database and [bcrypt](https://en.wikipedia.org/wiki/Bcrypt) or
[scrypt](https://en.wikipedia.org/wiki/Scrypt) if you're doing it by hand.
* Implement real sessions, that survive server restarts and fleets with multiple hosts.
* Consider removing the name from the `session` provider (and `authenticate`d routes that use it), for convenience.