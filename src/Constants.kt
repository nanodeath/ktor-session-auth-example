package name.maxaller

import io.ktor.auth.UserIdPrincipal

object FormFields {
    const val USERNAME = "username"
    const val PASSWORD = "password"
}

object AuthName {
    const val SESSION = "session"
    const val FORM = "form"
}

object CommonRoutes {
    const val LOGIN = "/login"
    const val LOGOUT = "/logout"
    const val PROFILE = "/profile"
}

object Cookies {
    const val AUTH_COOKIE = "auth"
}

object TestCredentials {
    const val USERNAME = "foo"
    const val PASSWORD = "bar"
}

/**
 * You can use whatever type you want to store the user id in; I've aliased it here to follow more easily.
 * Used in the cookie config, session auth config, and routes.
 * Whatever you choose here, it should implement [io.ktor.auth.Principal].
 */
typealias ExamplePrincipal = UserIdPrincipal

